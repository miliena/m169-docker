# Modul 169

- [Modul 169](#modul-169)
  - [Projekte](#projekte)
  - [Skripts](#skripts)

## Projekte

Hier die Links zu den Projektordner:

- [Adminer](./adminer/)
- [Rocket.Chat Chatumgebung](./rocketchat/chat/)
- [Rocket.Chat Monitoring](./rocketchat/monitoring/)

In jedem Ordner sind die dazugehörige Compose-Datei und eventuell noch Konfigurationsdateien.

## Skripts

Hier die Links zu den Skripts:

- [Docker Compose auf Ubuntu installieren](./scripts/install-compose.sh)
- [Public-DNS einer AWS-EC2-Instanz bei der Reverse Proxy Konfiguration einfügen](./scripts/insert-public-dns.sh) **funktioniert aktuell nicht 100%**

Beide Skripts werden von Cloud Init benötigt.
