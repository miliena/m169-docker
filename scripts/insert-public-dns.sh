#!/bin/bash

# copy config outside repo to change it
cp /m169-docker/"$1"/reverse-proxy.conf /reverse-proxy.conf

# Retrieve the public DNS name
public_dns=$(curl -s http://169.254.169.254/latest/meta-data/public-hostname)

# Path to proxy config
config=/reverse-proxy.conf

# Ensure the public DNS is retrieved successfully
if [ -n "$public_dns" ]; then
    # Update the configuration file with the public DNS
    if grep -q "PUBLIC-DNS" "$config"; then
        # Replace placeholder with the actual public DNS
        sed -i "s|PUBLIC-DNS|$public_dns|g" "$config"
        echo "Public DNS '$public_dns' inserted into $config."
    else
        echo "Error: Placeholder 'PUBLIC-DNS' not found in $config."
    fi
else
    echo "Error: Failed to retrieve public DNS."
fi
